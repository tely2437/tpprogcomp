import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Container, makeStyles } from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Badge from '@material-ui/core/Badge';
import axios from "axios";
import { useHistory } from "react-router-dom";
import {connect} from 'react-redux';
import {logout,setCurrency} from '../redux/auth/actions';
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";


const useStyles = makeStyles((theme) => ({
    toolbar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbarTitle: {
        flex: 1,
    },
    toolbarSecondary: {
        justifyContent: 'space-between',
        overflowX: 'auto',
    },
    toolbarLink: {
        padding: theme.spacing(1),
        flexShrink: 0,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 80,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const  Header  = (props) => {
    const history = useHistory();
    const classes = useStyles();
    const [classiquesCategories, setClassiquesCategories] = useState([]);
    const [encheresCategories, setEncheresCategories] = useState([]);

    const loadClassiquesCategories = useCallback(() => {
        axios.get(`http://localhost:8082/api/categories/1/sale-type`).then((response) => {
            setClassiquesCategories(response.data)
        });
    }, []);

    const loadEncheresCategories = useCallback(() => {
        axios.get(`http://localhost:8082/api/categories/2/sale-type`).then((response) => {
            setEncheresCategories(response.data)
        });
    }, []);


    const accountHandle = () =>{
        window.location.href="/account";
    }

    const adminHandle = () =>{
        window.location.href="/admin";
    }

    const loggout = () => {
        props.logout()
        history.push('/login');
    }

    const handleChange = (value) => {
        props.setCurrency(value.target.value)

    }

    useEffect(() => {
        loadClassiquesCategories();
        loadEncheresCategories();
    }, [loadClassiquesCategories,loadEncheresCategories]);


    if (props.auth.isLoggedIn
        && (history.location.pathname != "/admin"
            && history.location.pathname != "/admin/articles"
            && history.location.pathname != "/admin/categories")
        && props.auth.userType !=1 ){
        return (
            <React.Fragment>
                <Toolbar className={classes.toolbar}>
                    <Typography
                        component="h2"
                        variant="h5"
                        color="inherit"
                        align="center"
                        noWrap
                        className={classes.toolbarTitle}
                    >
                        Ecommerce
                    </Typography>
                    <IconButton>
                        <>
                            <FormControl className={classes.formControl}>
                                <InputLabel id="demo-simple-select-label">Monnaie</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={props.auth.currency}
                                    onChange={handleChange}
                                >
                                    <MenuItem value={'EURO'}>€</MenuItem>
                                    <MenuItem value={'DOL'}>$</MenuItem>
                                </Select>
                            </FormControl>
                        </>
                    </IconButton>

                            {props.auth.userType == 3 ? (
                                <>
                                    <Button variant="contained" onClick={accountHandle} variant="outlined" size="small">
                                        Mon compte
                                    </Button>
                                    <Button variant="contained" onClick={adminHandle} variant="outlined" size="small">
                                        Admin
                                    </Button>
                                    <Button variant="contained" onClick={loggout} variant="outlined" size="small">
                                        Se deconnecter
                                    </Button>
                                </>
                            ):(props.auth.userType == 2 ? (
                                <>
                                    <Button variant="contained" onClick={accountHandle} variant="outlined" size="small">
                                        Mon compte
                                    </Button>
                                    <Button variant="contained" onClick={loggout} variant="outlined" size="small">
                                        Se deconnecter
                                    </Button>
                                </>
                            ) : (
                                <>
                                    <Button onClick={adminHandle} variant="outlined" size="small">
                                        Admin
                                    </Button>
                                    <Button onClick={loggout} variant="outlined" size="small">
                                        Se deconnecter
                                    </Button>
                                </>
                            )) }



                </Toolbar>
                <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
                    <Link
                        color="inherit"
                        noWrap20
                        key={'Home'}
                        variant="body2"
                        href={`/`}
                        className={classes.toolbarLink}
                    >
                        Home
                    </Link>
                    {classiquesCategories.map((section) => (
                        <Badge badgeContent={'D'} color="primary">

                        <Link
                            color="inherit"
                            noWrap20
                            key={section.name}
                            variant="body2"
                            href={`/classiques/${section.id}`}
                            className={classes.toolbarLink}
                        >
                            {section.name}
                        </Link>
                        </Badge>
                    ))}

                    {encheresCategories.map((section) => (
                        <Badge badgeContent={'E'} color="error">

                        <Link
                            color="inherit"
                            noWrap20
                            key={section.name}
                            variant="body2"
                            href={`/encheres/${section.id}`}
                            className={classes.toolbarLink}
                        >
                            {section.name}
                        </Link>
                        </Badge>
                    ))}

                </Toolbar>
            </React.Fragment>
        );
    }else {
        return <div></div>
    }
}

const mapStateToProps = (store) => {
    return {
        auth: store.loginReducer,
    };
};

const mapDispatchToProps = {
    logout,
    setCurrency
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);

// export default Header;

Header.propTypes = {
    sections: PropTypes.array,
    title: PropTypes.string,
};
