import React, {useRef, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import {connect} from "react-redux";
import {getCat} from "../../redux/categorie/actions";

const FormDialogCategorie = (props) => {
    const [open, setOpen] = React.useState(false);
    const [name, setName] = React.useState('');
    const [isEnchere, setIsEnchere] = React.useState(false);

    const handleChange = (value) => {
        if(value.target.id =='name'){
            setName(value.target.value)
        }

    }

    const handleChecked = (el) => {
        setIsEnchere(el.target.checked)
    }

    const handleSubmit = () => {
        let data = {
            name: name,
            saleType: isEnchere ? 2 : 1
        }

        props.onSubmit(data);

        handleClose();

    }
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setName('');
        setIsEnchere(false)
    };

    useEffect(() => {
        props.handleRef.current = handleClickOpen;
        props.getCat()

        return () => {
            props.handleRef.current = null;
        };
    }, []);

    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Création Categorie</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Nom"
                        type="email"
                        required={true}
                        onChange={(el)=> handleChange(el)}
                        fullWidth
                    />
                    <FormControlLabel
                        control={<Checkbox checked={isEnchere} onChange={handleChecked} name="enchere" />}
                        label="Enchere"
                    />

                </DialogContent>
                <DialogActions>
                    <Button variant="contained" onClick={handleClose} color="primary">
                        Annuler
                    </Button>
                    <Button variant="contained" onClick={handleSubmit} color="primary">
                        Valider
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}


const mapStateToProps = (store) => {
    return {
        data: store.categoriesReducer,
    };
};
const mapDispatchToProps = {
    getCat
};
export default connect(mapStateToProps, mapDispatchToProps)(FormDialogCategorie);


