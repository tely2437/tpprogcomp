import React, {useCallback, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import axios from "axios";
import {
    useHistory,
    useParams
} from "react-router-dom";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {logout, setCurrency} from "../redux/auth/actions";
import {connect} from "react-redux";


const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));
const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const Encheres = (props) => {
    const classes = useStyles();
    const {id} = useParams();
    const [data, setData] = useState([])
    const [cat, setCat] = useState([])
    const [range, setRange] = React.useState('');

    const handleChange = (event) => {
        setRange(event.target.value)
        if (event.target.value == 20){
            let filtred = data
            filtred.sort((a, b)=>{
                return a.price - b.price;
            })
            setData(filtred)
        }
        if (event.target.value == 10){
            let filtred = data
            filtred.sort((a, b)=>{
                return b.price - a.price;
            })
            setData(filtred)
        }
    };
    const history = useHistory()

    const loadArticles = useCallback(() => {
        axios.get(`http://localhost:8082/api/categories/${id}`).then((response) => {
            setData(response.data.articles)
            setCat(response.data)

        });
    }, []);

    useEffect(() => {
        loadArticles();
    }, [loadArticles]);

    const onView = (id) =>{
        history.push(`/categorie/${cat.id}/article/view/${id}`)
    }
    return (
        <React.Fragment>
            <CssBaseline />
            <main>
                {/* Hero unit */}
                <div className={classes.heroContent}>
                    <Container maxWidth="sm">
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Vente au encheres
                        </Typography>
                        <Typography variant="h5" align="center" color="textSecondary" paragraph>
                            Cette Categorie est en Enchere.
                            Vente des article aux encheres avec un prix de depart, etc.

                        </Typography>
                    </Container>
                </div>
                <Container className={classes.cardGrid} maxWidth="md">
                    <FormControl className={classes.formControl}>
                        <InputLabel id="demo-simple-select-label">Filtre Prix</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={range}
                            onChange={handleChange}
                        >
                            <MenuItem value={10}>Descendant</MenuItem>
                            <MenuItem value={20}>Ascendant</MenuItem>
                        </Select>
                    </FormControl>
                    <br/>
                    <br/>
                    <br/>
                    {/* End hero unit */}
                    <Grid container spacing={4}>
                        {data.map((card) => (
                            <Grid item key={card} xs={12} sm={6} md={4}>
                                <Card className={classes.card}>
                                    <CardMedia
                                        className={classes.cardMedia}
                                        image="https://source.unsplash.com/random"
                                        title="Image title"
                                    />
                                    <CardContent className={classes.cardContent}>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {card.name}
                                        </Typography>
                                        <Typography>
                                            {card.description}
                                        </Typography>
                                        <Typography>
                                          Prix depart: {props.auth.currency == 'EURO' ? `€${card.price}` : `$${card.price * 1.21}`}
                                        </Typography>

                                        <Typography>
                                            {props.auth.currency == 'EURO' ?(
                                                card.currentPrice ? `Offre Actuelle: €${card.currentPrice}`:'Aucune offre'
                                            ):(
                                                card.currentPrice ? `Offre Actuelle: $${card.currentPrice * 1.21}`:'Aucune offre'
                                            )
                                                }
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button variant="contained" onClick={()=> onView(card.id)} size="small" color="primary">
                                            Voir
                                        </Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </Container>
            </main>
        </React.Fragment>
    )

}

const mapStateToProps = (store) => {
    return {
        auth: store.loginReducer,
    };
};

const mapDispatchToProps = {
    logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Encheres);

