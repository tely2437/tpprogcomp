import React, {useCallback, useEffect, useRef} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {Button} from "@material-ui/core";
import {DataGrid} from "@material-ui/data-grid";
import {getCat} from "../../redux/categorie/actions";
import {connect} from "react-redux";
import FormDialogCategorie from "../../components/FormDialogCategorie";
import axios from "axios";
import {toast} from "react-toastify";


const columns = [
    { field: 'id', headerName: 'ID', width: 200 },
    { field: 'name', headerName: 'Titre', width: 200 },
    { field: 'saleType', headerName: 'Type de Vente', width: 200 },
]
const Categories = (props) => {
    const handleRef = useRef();

    const loadCategories = useCallback(  () => {
        props.getCat()
    }, [])

    const onSubmit = (values) =>{
            axios.post(`http://localhost:8082/api/categories/`,values)
                .then((response)=>{
                    console.log('values::',response)
                    toast.success('success')
                    props.getCat()
                }).catch((error)=>console.log('error'))

    }
    const handleClickOpen = () => handleRef.current();

    useEffect(() => {
        loadCategories()
        console.log(props.data)
    }, [loadCategories]);


    return (
        <div style={{ height: 400, width: '100%' }}>
            <div>GESTION CATEGORIE</div>
            <FormDialogCategorie onSubmit={onSubmit} handleRef={handleRef}/>

            <div>
                <Button variant="contained" onClick={handleClickOpen}>Ajouter</Button>
            </div>
            <DataGrid rows={props.data.categories} columns={columns} pageSize={5} />
        </div>
    )

}

const mapStateToProps = (store) => {
    return {
        data: store.categoriesReducer,
    };
};

const mapDispatchToProps = {
    getCat
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
