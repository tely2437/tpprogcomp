import React, {useEffect, useState, useCallback, useRef} from 'react';
import { toast } from 'react-toastify'

import {DataGrid} from "@material-ui/data-grid";
import axios from "axios"
import {connect} from "react-redux";
import {get} from "../../redux/article/actions";
import {Button} from "@material-ui/core";

import FormDialogArticle from "../../components/FormDialogArticle";

const columns = [
    { field: 'id', headerName: 'ID', width: 200 },
    { field: 'name', headerName: 'Titre', width: 200 },
    { field: 'description', headerName: 'Description', width: 200 },
    {
        field: 'color',
        headerName: 'Couleur',
        width:200,
    },
    {
        field: 'price',
        headerName: 'Prix',
        width: 200,
    },

    {
        field: 'type',
        headerName: 'Etat',
        width: 200,
    },

]

const Articles = (props) => {
    const handleRef = useRef();

    const loadArticles = useCallback(  () => {
       props.get()
    }, [])

    const handleClickOpen = () => handleRef.current();

    useEffect(() => {
        loadArticles()
    }, [loadArticles]);

    const onSubmit = (values) =>{
        if (values.id){
            axios.post(`http://localhost:8082/api/categories/${values.id}/articles/`,values)
                .then((response)=>{
                    console.log('values::',response)
                    props.get()
                }).catch((error)=>console.log('error'))
        }else{
            toast.error('error')

        }

    }

    return (
        <div style={{ height: 400, width: '100%' }}>
            <div>GESTION ARTICLE</div>

            <FormDialogArticle onSubmit={onSubmit} handleRef={handleRef}/>
            <div>
                <Button variant="contained" onClick={handleClickOpen}>Ajouter</Button>
            </div>
            <DataGrid rows={props.data.articles} columns={columns} pageSize={5} />
        </div>
    )

}

const mapStateToProps = (store) => {
    return {
        data: store.articlesReducer,
    };
};

const mapDispatchToProps = {
    get
};
export default connect(mapStateToProps, mapDispatchToProps)(Articles);

