import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {logout} from "../redux/auth/actions";
import {connect} from "react-redux";
import { useHistory } from "react-router-dom";

const Home = (props) => {
    const history = useHistory();

    const cheickAuth = () => {
        if (!props.auth.isLoggedIn){
            history.push('/login');
        }
        if (props.auth.userType == 1){
            history.push('/admin')
        }
    }
    useEffect(() => {
        cheickAuth()
        console.log("history",history)
    }, []);



    return (
        <Container>
            <div>
                Bienvenue sur notre site vente au enchere Les categorie sont listé sous forme menu
            </div>
        </Container>
    )

}

const mapStateToProps = (store) => {
    return {
        auth: store.loginReducer,
    };
};

const mapDispatchToProps = {
    logout
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);


