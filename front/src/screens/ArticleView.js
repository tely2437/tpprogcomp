import React, {useCallback, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import {useParams} from "react-router-dom";
import axios from "axios";
import {Button} from "@material-ui/core";
import {logout, setLoginState} from "../redux/auth/actions";
import {connect} from "react-redux";
import {toast} from "react-toastify";
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(10),
        margin: 'auto',
        maxWidth: '95%',
        maxHeight: '100%',
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
}));


const ArticleView = (props) => {
    const classes = useStyles();
    const {id} = useParams();
    const {catId} = useParams();

    const [data, setData] = useState(null)
    const [cat, setCat] = useState(null)
    const [price, setPrice] = useState(0)
    const [disabled, setDisabled] = useState(false)
    const loadArticle = useCallback(() => {
        axios.get(`http://localhost:8082/api/articles/${id}`).then((response) => {
            setData(response.data)
        });
    }, []);

    const loadCat = useCallback(() => {
        axios.get(`http://localhost:8082/api/categories/${catId}`).then((response) => {
            setCat(response.data)
        });
    }, []);

    useEffect(() => {
        loadArticle();
        loadCat();
    }, [loadArticle,loadCat]);

    const onAcheter = () => {
        axios.put(`http://localhost:8082/api/articles/${data.id}`,{
            type: 'vendu',
        }).then((response) => {
            setData(response.data)
            axios.post(`http://localhost:8082/api/user/${props.auth.id}/articles/${data.id}`).then((response)=>{
                props.setLoginState(response.data)
                toast.success('Article Acheté')
            }).catch((err)=> console.log(err))
        });
    }
    const onEncherir = () => {
        axios.put(`http://localhost:8082/api/articles/${data.id}`,{
            type: 'enchere',
            currentPrice: price
        }).then((response) => {
            setData(response.data)
            if (data.type != 'enchere'){
                axios.post(`http://localhost:8082/api/user/${props.auth.id}/articles/${data.id}`).then((response)=>{
                    props.setLoginState(response.data)
                }).catch((err)=> console.log(err))
            }

            toast.success('Enchere Posée')

        });
    }


    return (
        <React.Fragment>
            <CssBaseline />
            <main>
                <div className={classes.heroContent}>
                    <Container maxWidth="sm">
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            {data && data.name}
                        </Typography>
                        <Typography variant="h5" align="center" color="textSecondary" paragraph>
                            {data && data.description}
                        </Typography>
                    </Container>
                </div>
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container spacing={3}>
                    <Grid item>
                        <ButtonBase className={classes.image}>
                            <img className={classes.img} alt="complex" src="https://source.unsplash.com/random" />
                        </ButtonBase>
                    </Grid>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography gutterBottom variant="subtitle1">
                                    {data && data.type =='enchere' ? 'Enchere' : ''}
                                    {data && data.type =='vendu' ? 'Vendu' : ''}
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    {cat && cat.saleType == 1 ?
                                        'Ce produit est en achat direct' :
                                        'Ce produit est au enchere'}
                                </Typography>
                                <Typography variant="body2" color="textSecondary">
                                    {(props.auth.currency == 'EURO' ) ? (
                                        cat && cat.saleType == 2 ?
                                            `Enchere Actuelle: €${data && data.currentPrice}` :
                                            ''
                                    ) : (
                                        cat && cat.saleType == 2 ?
                                            `Enchere Actuelle: $${data && data.currentPrice * 1.21}` :
                                            ''
                                    )}
                                </Typography>
                            </Grid>
                            <Grid item>

                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography variant="subtitle1">
                                {
                                    cat && cat.saleType == 2 ? (
                                        ' Prix de depart: '
                                    ): (
                                        ''
                                    )
                                }
                                €{data && ((props.auth.currency == 'EURO' ) ?`€${data.price}`:`$${data.price * 1.21}`)}
                            </Typography>
                            <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                {
                                    cat && cat.saleType == 2 ? (
                                        <TextField
                                            autoFocus
                                            margin="dense"
                                            id="name"
                                            label="Votre prix"
                                            type="number"
                                            required={true}
                                            onChange={(el)=> setPrice(el.target.value)}
                                            fullWidth
                                        />
                                    ): (
                                        ''
                                    )
                                }
                                {cat && cat.saleType == 1 ?
                                    (<Button disabled={data ? data.type =='vendu':false} variant="contained" onClick={onAcheter}>Acheter</Button>) :
                                    (<Button disabled={disabled} variant="contained" onClick={onEncherir}>Encherir</Button>)}

                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </div>
            </main>
        </React.Fragment>
    );


}

const mapStateToProps = (store) => {
    return {
        auth: store.loginReducer,
    };
};

const mapDispatchToProps = {
    logout,
    setLoginState
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleView);


// export  default  ArticleView;
