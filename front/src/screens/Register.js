
import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from  "axios";
import {toast} from "react-toastify";
import {useHistory} from "react-router-dom";
const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const Register =(props)=>{
    const classes = useStyles();
    const history = useHistory();


    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isSaler, setIsSaler] = React.useState(false);
    const [isBuyer, setIsBuyer] = React.useState(false);

    const onSubmit = (event) => {
        event.preventDefault();
        let type = 1;
        if (isSaler){
            if (isBuyer){
                type = 3
            }
        }else {
            if (isBuyer){
                type = 2
            }
        }

        axios.post('http://localhost:8082/api/user/',{
            email: email,
            password: password,
            userType:  type
        }).then((response)=>{
            toast.success('Login success')
            history.push('/login')
        }).catch((err)=>{
            toast.error('error login')
        })


        console.log('type',type)
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign up
                </Typography>
                <form className={classes.form} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                value={email}
                                onChange={(el)=>setEmail(el.target.value)}
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                onChange={(el)=>setPassword(el.target.value)}
                                value={password}
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControlLabel
                                control={<Checkbox
                                    checked={isSaler}
                                    onChange={(el)=> setIsSaler(el.target.checked)}
                                    value="allowExtraEmails"
                                    color="primary" />}
                                label="Vendeur"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControlLabel
                                control={<Checkbox
                                    checked={isBuyer}
                                    onChange={(el)=> setIsBuyer(el.target.checked)}
                                    value="allowExtraEmails"
                                    color="primary" />}
                                label="Acheteur"
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        onClick={onSubmit}
                        className={classes.submit}
                    >
                        Sign Up
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link href="/login" variant="body2">
                                Vous avez déja un compte? Login
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>

        </Container>
    );
}

export default Register
