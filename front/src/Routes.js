import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import Encheres from "./screens/Encheres";
import Classiques from "./screens/Classiques";
import Home from "./screens/Home";
import Login from "./screens/Login";
import Admin from "./screens/Admin";
import Account from "./screens/Account";
import ArticleView from "./screens/ArticleView";
import Register from "./screens/Register";

const Routes = () => {
    return (
            <Switch>
                <Route exact path="/">
                    <Home/>
                </Route>

                <Route path="/admin">
                    <Admin/>
                </Route>

                <Route path="/login">
                    <Login/>
                </Route>

                <Route path="/encheres/:id">
                    <Encheres/>
                </Route>

                <Route path="/classiques/:id">
                    <Classiques/>
                </Route>
                <Route path="/account">
                    <Account/>
                </Route>

                <Route path="/register">
                    <Register/>
                </Route>

                <Route path="/categorie/:catId/article/view/:id">
                    <ArticleView/>
                </Route>
            </Switch>
    )
}

export default Routes;
