import * as t from '../actionTypes';
import axios from 'axios';
import { toast } from 'react-toastify'

const LoginUrl = 'http://localhost:8082/api/authentication/login';

export const login = (loginInput) => async (dispatch) => {
    // don't forget to use dispatch here!
    try {

        const response = await axios.post(LoginUrl, loginInput);
        dispatch(setLoginState(response.data));
        toast.success('Login success')

    } catch (error) {
        toast.error('Login error')

    }
};

export const logout = () => async (dispatch) => {
    dispatch(setLogoutState());
};

export const setLoginState = (loginData) => {
    return {
        type: t.SET_LOGIN_STATE,
        payload: loginData,
    };
};

export const setCurrency = (currency) => {
    return {
        type: t.SET_CURRENCY,
        payload: currency,
    };
};

const setLogoutState = () => {
    return {
        type: t.SET_LOGOUT,
        payload: {},
    };
};
