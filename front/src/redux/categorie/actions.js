import * as t from '../actionTypes';
import axios from 'axios';
import { toast } from 'react-toastify'

const LoginUrl = 'http://localhost:8082/api/categories/';

export const getCat = () => async (dispatch) => {
    // don't forget to use dispatch here!
    try {

        const response = await axios.get(LoginUrl);
        dispatch(setCategoriesState(response.data));
        toast.success('Categories get success')

    } catch (error) {
        toast.error('Categories get error')

    }

};

const setCategoriesState = (data) => {
    return {
        type: t.SET_CATEGORIES_STATE,
        payload: data,
    };
};
