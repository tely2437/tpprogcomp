import * as t from '../actionTypes';
import axios from 'axios';
import { toast } from 'react-toastify'

const LoginUrl = 'http://localhost:8082/api/articles/';

export const get = () => async (dispatch) => {
    // don't forget to use dispatch here!
    try {

        const response = await axios.get(LoginUrl);
        dispatch(setArticlesState(response.data));
        toast.success('Articles get success')

    } catch (error) {
        toast.error('Articles get error')

    }

};

const setArticlesState = (data) => {
    return {
        type: t.SET_ARTICLES_STATE,
        payload: data,
    };
};
