import {initialState} from './initialState';
import * as t from '../actionTypes';

export const articlesReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.SET_ARTICLES_STATE:
            return {
                ...state,
                articles: action.payload, // this is what we expect to get back from API call and login page input
            };
        default:
            return state;
    }
};
