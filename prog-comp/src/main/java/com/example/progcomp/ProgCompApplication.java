package com.example.progcomp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;



@SpringBootApplication
@EntityScan("com.example.progcomp.model")
@PropertySource(value="classpath:application.properties")
@EnableJpaRepositories("com.example.progcomp.repository")
@EnableAutoConfiguration(exclude = {
		SecurityAutoConfiguration.class
		})
public class ProgCompApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgCompApplication.class, args);
	}

}
