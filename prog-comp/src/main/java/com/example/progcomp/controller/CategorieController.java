package com.example.progcomp.controller;

import com.example.progcomp.model.Categorie;
import com.example.progcomp.model.CategorieDTO;
import com.example.progcomp.model.Article;
import com.example.progcomp.model.ArticleDTO;
import com.example.progcomp.service.CategorieService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.List;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin(origins = "http://localhost:3000")
//@CrossOrigin(origins = "https://prog-comp-front.hello-taxi-entreprise.fr")
public class CategorieController {
    private final  CategorieService categorieService;

    @GetMapping("/")
    @ApiOperation(value="View a list of all categories", response = Categorie.class, responseContainer = "List")
    public ResponseEntity<?> getAllCategories(){
        try {
            return new ResponseEntity<>(
                    categorieService.getAllCategories(),
                    HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @GetMapping("/{id}")
    @ApiOperation(value="Find a categorie info by its id", response = Categorie.class)
    public ResponseEntity<?> getCategorie(@PathVariable Long id){
        try {
            Optional<Categorie> optCategorie = categorieService.getCategorieById(id);
            if (optCategorie.isPresent()) {
                return new ResponseEntity<>(
                        optCategorie.get(),
                        HttpStatus.OK);
            } else {
                return noCategorieFoundResponse(id);
            }
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @GetMapping("/{saleType}/sale-type")
    @ApiOperation(value="Find a categorie info by its saleType", response = Categorie.class)
    public ResponseEntity<?> getCategorieBySaleType(@PathVariable Integer saleType){
        try {
            return new ResponseEntity<>(
                    categorieService.getAllCategorieBySaleType(saleType),
                    HttpStatus.OK);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PostMapping("/")
    @ApiOperation(value="Save new Categorie", response = Categorie.class)
    public ResponseEntity<?> createCategorie(@RequestBody CategorieDTO categorieDTO){
        try {
            return new ResponseEntity<>(
                    categorieService.saveNewCategorie(categorieDTO),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value="Update a Categorie with specific id", response = Categorie.class)
    public ResponseEntity<?> updateCategorie(@PathVariable Long id, @RequestBody CategorieDTO categorieDTO){
        try {
            Optional<Categorie> optCategorie = categorieService.getCategorieById(id);
            if (optCategorie.isPresent()) {
                return new ResponseEntity<>(
                        categorieService.updateCategorie(optCategorie.get(), categorieDTO),
                        HttpStatus.OK);
            } else {
                return noCategorieFoundResponse(id);
            }
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value="Delete Categorie with specific id", response = String.class)
    public ResponseEntity<?> deleteCategorie(@PathVariable Long id){
        try {
            Optional<Categorie> optCategorie = categorieService.getCategorieById(id);
            if (optCategorie.isPresent()) {
                categorieService.deleteCategorie(optCategorie.get());
                return new ResponseEntity<>(
                        String.format("Categorie with id: %d was deleted", id),
                        HttpStatus.OK);
            } else {
                return noCategorieFoundResponse(id);
            }
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @GetMapping("/{categorieId}/articles/")
    @ApiOperation(value="View a list of all articles for a Categorie with provided id", response = Article.class, responseContainer = "List")
    public ResponseEntity<?> getAllArticlesInCategorie(@PathVariable Long categorieId){
        try {
            Optional<Categorie> optCategorie = categorieService.getCategorieById(categorieId);
            if (optCategorie.isPresent()) {
                return new ResponseEntity<>(
                        optCategorie.get().getArticles(),
                        HttpStatus.OK);
            } else {
                return noCategorieFoundResponse(categorieId);
            }
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PostMapping("/{categorieId}/articles/")
    @ApiOperation(value="Save new Article and assign it to Categorie", response = Categorie.class)
    public ResponseEntity<?> createArticleAssignedToCategorie(@PathVariable Long categorieId, @RequestBody ArticleDTO articleDTO){
        try {
            return new ResponseEntity<>(
                    categorieService.addNewArticleToCategorie(categorieId, articleDTO),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    private ResponseEntity<String> errorResponse(){
        return new ResponseEntity<>("Something went wrong :(", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<String> noCategorieFoundResponse(Long id){
        return new ResponseEntity<>("No Categorie found with id: " + id, HttpStatus.NOT_FOUND);
    }
    private ResponseEntity<String> noCategorieSaleTypeFoundResponse(Integer id){
        return new ResponseEntity<>("No Categorie found with id: " + id, HttpStatus.NOT_FOUND);
    }
}