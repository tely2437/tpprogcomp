package com.example.progcomp.controller;

import com.example.progcomp.model.User;
import com.example.progcomp.model.UserDTO;
import com.example.progcomp.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import java.util.Optional;

@RestController
@RequestMapping("/authentication")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin(origins = "http://localhost:3000")
//@CrossOrigin(origins = "https://prog-comp-front.hello-taxi-entreprise.fr")

public class AuthenticationController {

    private final UserService userService;

    @PostMapping("/login")
    @ApiOperation(value="Auth", response = User.class)
    public ResponseEntity<?> auth(@RequestBody UserDTO userDTO){
        try {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            Optional<User> result = userService.getUserByEmail(userDTO.getEmail());

            if (result.isPresent()) {
                User data = result.get();

                boolean isOk = passwordEncoder.matches(userDTO.getPassword() , data.getPassword());
                if (isOk){

                    return new ResponseEntity<>(
                            data,
                            HttpStatus.CREATED);
                }
            }

            return noUserFoundResponse();

        } catch (Exception e) {
            return errorResponse();
        }
    }

//    @PostMapping("/create")
//    @ApiOperation(value="Save new article", response = User.class)
//    public ResponseEntity<?> createUser(@RequestBody UserDTO userDTO){
//        try {
//            return new ResponseEntity<>(
//                    userService.saveNewUser(userDTO),
//                    HttpStatus.CREATED);
//        } catch (Exception e) {
//            return errorResponse();
//        }
//    }


    private ResponseEntity<String> errorResponse(){
        return new ResponseEntity<>("Something went wrong :(", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<String> noUserFoundResponse(){
        return new ResponseEntity<>("No user found ", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}