package com.example.progcomp.controller;

import com.example.progcomp.model.Article;
import com.example.progcomp.model.ArticleDTO;
import com.example.progcomp.service.ArticleService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/articles")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin(origins = "https://prog-comp-front.hello-taxi-entreprise.fr")
//@CrossOrigin(origins = "http://localhost:3000")
public class ArticleController {

    private final ArticleService articleService;

    @GetMapping("/")
    @ApiOperation(value="View a list of all articles", response = Article.class, responseContainer = "List")
    public ResponseEntity<?> getAllArticles(){
        try {
            return new ResponseEntity<>(
                    articleService.getAllArticles(),
                    HttpStatus.OK
            );
        }catch (Exception e) {
            return errorResponse();
        }
    }

    @GetMapping("/{id}")
    @ApiOperation(value="Find a article info by its id", response = Article.class)
    public ResponseEntity<?> getArticle(@PathVariable Long id){
        try {
            Optional<Article> optArticle = articleService.getArticleById(id);
            if (optArticle.isPresent()) {
                return new ResponseEntity<>(
                        optArticle.get(),
                        HttpStatus.OK);
            } else {
                return noArticleFoundResponse(id);
            }
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PostMapping("/")
    @ApiOperation(value="Save new article", response = Article.class)
    public ResponseEntity<?> createArticle(@RequestBody ArticleDTO articleDTO){
        try {
            return new ResponseEntity<>(
                    articleService.saveNewArticle(articleDTO),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value="Update a article with specific id", response = Article.class)
    public ResponseEntity<?> updateArticle(@PathVariable Long id, @RequestBody ArticleDTO articleDTO){
        try {
            Optional<Article> optArticle = articleService.getArticleById(id);
            if (optArticle.isPresent()) {
                return new ResponseEntity<>(
                        articleService.updateArticle(optArticle.get(), articleDTO),
                        HttpStatus.OK);
            } else {
                return noArticleFoundResponse(id);
            }
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value="Delete Article with specific id", response = String.class)
    public ResponseEntity<?> deleteArticle(@PathVariable Long id){
        try {
            Optional<Article> optArticle = articleService.getArticleById(id);
            if (optArticle.isPresent()) {
                articleService.deleteArticle(optArticle.get());
                return new ResponseEntity<>(String.format("Article with id: %d was deleted", id), HttpStatus.OK);
            } else {
                return noArticleFoundResponse(id);
            }
        } catch (Exception e) {
            return errorResponse();
        }
    }


    private ResponseEntity<String> errorResponse(){
        return new ResponseEntity<>("Something went wrong :(", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<String> noArticleFoundResponse(Long id){
        return new ResponseEntity<>("No article found with id: " + id, HttpStatus.NOT_FOUND);
    }
}