package com.example.progcomp.controller;

import com.example.progcomp.model.User;
import com.example.progcomp.model.UserDTO;
import com.example.progcomp.model.Article;
import com.example.progcomp.model.ArticleDTO;
import com.example.progcomp.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import java.util.Optional;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
//@CrossOrigin(origins = "http://localhost:3000")
//@CrossOrigin(origins = "https://prog-comp-front.hello-taxi-entreprise.fr")
public class UserController {

    private final UserService userService;

    @PostMapping("/")
    @ApiOperation(value="Save new user", response = User.class)
    public ResponseEntity<?> createUser(@RequestBody UserDTO userDTO){
        try {
            return new ResponseEntity<>(
                    userService.saveNewUser(userDTO),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return errorResponse();
        }
    }

    @PostMapping("/{userId}/articles/{articleId}")
    @ApiOperation(value="Save new Article and assign it to User", response = User.class)
    public ResponseEntity<?> createArticleAssignedToUser(@PathVariable Long userId, @PathVariable Long articleId){
        try {
            return new ResponseEntity<>(
                    userService.addNewArticleToUser(userId, articleId),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return errorResponse();
        }
    }


    private ResponseEntity<String> errorResponse(){
        return new ResponseEntity<>("Something went wrong :(", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<String> noUserFoundResponse(){
        return new ResponseEntity<>("No user found ", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}