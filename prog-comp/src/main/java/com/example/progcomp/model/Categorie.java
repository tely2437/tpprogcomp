package com.example.progcomp.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@Table(name = "categorie")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Categorie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(position = 1)
    private Long id;

    @Column(name = "name")
    @ApiModelProperty(position = 2)
    private String name;

    @OneToMany(
            cascade = {CascadeType.ALL},
            fetch = FetchType.EAGER)
    @JoinColumn(name = "categorie_id")
    @ApiModelProperty(position = 3)
    private List<Article> articles;

    @Column(name = "sale_type")
    @ApiModelProperty(position = 4)
    private int saleType;

    public void addArticle(Article article) {

        if (Objects.isNull(articles)) {
            articles = new ArrayList<>();
        }
        articles.add(article);
    }
}