package com.example.progcomp.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@Table(name = "auth_user")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(position = 1)
    private Long id;

    @Column(name = "email")
    @ApiModelProperty(position = 2)
    private String email;


    @Column(name = "password")
    @ApiModelProperty(position = 2)
    private String password;

    @Column(name = "user_type")
    @ApiModelProperty(position = 3)
    private int userType;

    @OneToMany(
            cascade = {CascadeType.ALL},
            fetch = FetchType.EAGER)
    @JoinColumn(name = "buyer_id")
    @ApiModelProperty(position = 3)
    private List<Article> articles;


    public void addArticle(Article article) {

        if (Objects.isNull(articles)) {
            articles = new ArrayList<>();
        }
        articles.add(article);
    }


}