package com.example.progcomp.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "article")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(position = 1)
    private Long id;

    @Column(name = "name")
    @ApiModelProperty(position = 2)
    private String name;

    @Column(name = "description")
    @ApiModelProperty(position = 3)
    private String description;

    @Column(name = "color")
    @ApiModelProperty(position = 4)
    private String color;

    @Column(name = "type")
    @ApiModelProperty(position = 5)
    private String type;

    @Column(name = "price")
    @ApiModelProperty(position = 6)
    private double price;

    @Column(name = "current_price")
    @ApiModelProperty(position = 7)
    private double currentPrice;


}