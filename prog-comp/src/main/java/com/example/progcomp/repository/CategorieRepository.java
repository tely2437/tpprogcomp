package com.example.progcomp.repository;

import com.example.progcomp.model.Categorie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.List;

@Repository
public interface CategorieRepository extends CrudRepository<Categorie, Long> {

    Optional<Categorie> findByName(String name);
    List<Categorie> findAllBySaleType(Integer saleType);
}
